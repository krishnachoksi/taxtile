import { Component } from "@angular/core";
import { NavController, PopoverController, Events } from "ionic-angular";
import { LoginPage } from '../../pages/login/login';
import { OrdersPage } from '../../pages/orders/orders';
import { ProductsPage } from '../../pages/products/products';
import { MembersPage } from '../../pages/members/members';
import { IonicUtilProvider } from '../../providers/ionic-util/ionic-util';
import { Utility } from '../../app/core/utility';
import { CommonService } from './../../app/core/service/common.service';
import { NotificationPage } from "../../pages/notification/notification";
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})

export class HomePage {

	userData: any;
	menuItems: Array<any> = [];
	component: any;
	orders = 0;
	products = 0;
	members = 0;
	constructor(public events: Events,
		public nav: NavController, public popoverCtrl: PopoverController,
		public ionic: IonicUtilProvider, public common: CommonService) {
		this.userData = JSON.parse(localStorage.getItem('.user_data'));

		if (this.userData !== null) {
			if(this.userData.user_type !== 'admin'){
				this.nav.setRoot(OrdersPage);
			}
		}
		else{
			this.nav.setRoot(LoginPage);
		}
		this.getCounts();
		this.loginUser(this.userData);
		//Admin Home page
		if (this.userData.user_type === 'admin') {
			this.menuItems = [
				{
					title: 'Orders',
					icon: './assets/images/menu/order.png',
					label: this.orders,
					component: OrdersPage
				},
				{
					title: 'Products',
					icon: './assets/images/menu/product.png',
					label: this.products,
					component: ProductsPage
				},
				{
					title: 'Members',
					icon: './assets/images/menu/users.png',
					label: this.members,
					component: MembersPage
				}
			];
		}
		//Marketing home page
		if (this.userData.user_type === 'marketing') {
			this.menuItems = [
				{
					title: 'Orders',
					icon: './assets/images/menu/order.png',
					component: OrdersPage
				}
			];
		}
		//Digital machine home page
		if (this.userData.user_type === 'digitalmachine') {
			this.menuItems = [
				{
					title: 'Orders',
					icon: './assets/images/menu/order.png',
					component: OrdersPage
				}
			];
		}
		//Polishing machine home page
		if (this.userData.user_type === 'polishingmachine') {
			this.menuItems = [
				{
					title: 'Orders',
					icon: './assets/images/menu/order.png',
					component: OrdersPage
				}
			];
		}

	}

	openPage(item) {
		this.nav.push(item.component);
	}

	ionViewWillEnter() {
		this.getCounts();
	}

	getCounts() {
		this.common.getCounts().subscribe(res => {
			console.log(res);
			if (res['status'] == "1") {
				this.orders = res["data"]["orders"];
				this.members = res["data"]["users"];
				this.products = res["data"]["product"];
				console.log(this.orders);
			}
		},
			error => {
				this.ionic.showToastAlert(error['error'].message);
			})
	}

	loginUser(user) {
		this.events.publish('user:login', user);
	}

	logout() {
		Utility.removeLS('user_data');
		Utility.removeLS('token');
		Utility.removeLS('loginValue');
		Utility.removeLS('password');
		this.nav.setRoot(LoginPage);
	}

	presentNotifications(myEvent) {
		console.log(myEvent);
		let popover = this.popoverCtrl.create(NotificationPage);
		popover.present({
			ev: myEvent
		});
	}
}
