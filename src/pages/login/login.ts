import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, Events } from 'ionic-angular';
import { EmailValidator } from '../../validators/email';
import { FormBuilder, Validators } from '@angular/forms';
import { HomePage } from '../../pages/home/home';
import { LoginService } from '../../app/core/service/login.service';
import { IonicUtilProvider } from '../../providers/ionic-util/ionic-util';
import { Utility } from '../../app/core/utility';
import { CommonService } from './../../app/core/service/common.service';
import { OrdersPage } from '../orders/orders';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {
	public loginForm;
	loading: Loading;
	user_data: any;
	data: { forgot_email: any };
	emailData: any;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public formBuilder: FormBuilder,
		public alertCtrl: AlertController, public loadingCtrl: LoadingController,
		public login: LoginService, public ionic: IonicUtilProvider,
		public common: CommonService, public events: Events) {
		this.user_data = JSON.parse(localStorage.getItem('.user_data'));
		// console.log(this.user_data['userDetail'].user_type);
		if (this.user_data != null) {
			this.navCtrl.setRoot(HomePage);
		}
		this.loginForm = this.formBuilder.group({
			email: ['Admin', Validators.compose([Validators.required])],
			password: ['12345678', Validators.compose([Validators.minLength(6), Validators.required])]
		});
	}

	ionViewDidLoad() { }

	loginUser() {
		if (this.loginForm.valid) {
			this.ionic.loadingShow();
			this.login.login(this.loginForm.value).subscribe(res => {
				console.log(res);
				if (res['status'] == 1) {
					Utility.setLS('user_data', JSON.stringify(res['data']['userDetail']));
					Utility.setLS('token', JSON.stringify(res['data']['loginToken']['token']));
					Utility.setLS("password", btoa(this.loginForm.value.password));
					Utility.setLS("loginValue", JSON.stringify(this.loginForm.value));
					this.ionic.showToastAlert(res['message']);
					this.user_data = res["data"]["userDetail"];
					if(this.user_data.user_type == 'admin'){
						
						this.navCtrl.setRoot(HomePage);
					}
					else{
						this.events.publish('user:login',this.user_data);
						this.navCtrl.setRoot(OrdersPage);
					}
					this.ionic.loadingHide();
				}
				else {
					this.ionic.showToastAlert(res['message']);
					console.log("error");
				}
			},
				error => {
					console.log(error.error['message']);
					this.ionic.showToastAlert(error.error['message']);
				});
		}
		else {
			this.ionic.showToastAlert("Invalid Form details");
		}
	}

	forgotPass() {
		let forgot = this.alertCtrl.create({
			title: 'Forgot Password?',
			message: "Enter you email address to send a reset link password.",
			inputs: [
				{
					name: 'email',
					placeholder: 'Email',
					type: 'email',
					value: 'gamiparesh5@gmail.com',
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: data => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Send',
					handler: data => {
						console.log(data);
						let validateObj = this.validateEmail(data.email);
						if (!validateObj) {
							this.ionic.showToastAlert('Please enter valid email.');
						} else {
							this.sendMail(data.email);
						}
					}
				}
			]
		});
		forgot.present();
	}

	validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	sendMail(email) {
		this.data = { forgot_email: email }
		console.log(this.data);
		this.common.sendMail(this.data).subscribe(res => {
			console.log(res);
			if (res['status'] == 1) {
				this.emailData = res['data'];
				this.ionic.showToastAlert(res['message']);
			}
			else {
				this.ionic.showToastAlert(res['message']);
				console.log("error");
			}
		},
			error => {
				console.log(error.error['message']);
				this.ionic.showToastAlert(error.error['message']);
				this.ionic.loadingHide();
			});
	}
}
