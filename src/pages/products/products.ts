import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { LoginService } from '../../app/core/service/login.service';
import { IonicUtilProvider } from '../../providers/ionic-util/ionic-util';
import { CommonService } from './../../app/core/service/common.service';
import { HomePage } from '../home/home';
import { AddProductPage } from '../add-product/add-product';
@Component({
	selector: 'page-products',
	templateUrl: 'products.html',
})
export class ProductsPage {
	id: any;
	productData: any;
	userData: any;
	token: any;
	data: { id: any };
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public loadingCtrl: LoadingController, public alertCtrl: AlertController,
		public login: LoginService, public ionic: IonicUtilProvider,
		public common: CommonService) {

		this.userData = JSON.parse(localStorage.getItem('.user_data'));
		this.token = JSON.parse(localStorage.getItem('.token'));

		if (this.userData != null) {
			this.getAllProducts();
		}
		else {
			this.navCtrl.setRoot(LoginPage);
		}
	}

	ionViewDidLoad() {

	}

	home() {
		this.navCtrl.setRoot(HomePage);
	}

	getAllProducts() {
		this.id = this.userData['id'];
		this.common.getAllProducts().subscribe(res => {
			if (res['status'] == 1) {
				this.productData = res['data'];
			}
			else {
				this.ionic.showToastAlert(res['message']);
				console.log("error");
			}
		},
			error => {
				console.log("errorrr");
				this.ionic.showToastAlert("Error in getting product list.");
				this.ionic.loadingHide();
			});

	}

	openAddProduct() {
		this.navCtrl.push(AddProductPage);
	}

	editProduct(event,item){
		event.stopPropagation();
		this.navCtrl.push(AddProductPage, { product: item });
	}

	deleteProduct(event, id){
		event.stopPropagation();
		console.log(id);
		let alert = this.alertCtrl.create({
			title: 'Confirm Delete',
			message: 'Do you want to delete this product?It will delete all orders of this product too.!!',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {

					}
				},
				{
					text: 'Delete',
					handler: () => {
						this.ionic.loadingShow();
						this.data = { id: id };
						this.common.deleteProduct(this.data).subscribe(res => {
				
							if (res['status'] == 1) {
								this.ionic.showToastAlert(res['message']);
								this.getAllProducts();
								this.ionic.loadingHide();
							}
						},
							error => {
								this.ionic.showToastAlert("Error in getting member list.");
								this.ionic.loadingHide();
							});
					}
				}
			]
		});
		alert.present();
	}

}
