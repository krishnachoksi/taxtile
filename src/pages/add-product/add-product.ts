import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../../app/core/service/login.service';
import { IonicUtilProvider } from '../../providers/ionic-util/ionic-util';
import { CommonService } from './../../app/core/service/common.service';
import { ProductsPage } from '../products/products';
import { CustomValidators } from 'ng2-validation';

@Component({
    selector: 'page-add-product',
    templateUrl: 'add-product.html',
})
export class AddProductPage {
    loading: any;
    createSuccess = false;
    userData: any;
    public addProductForm;
    data: {
        id: any,
        design_number: any
        // product_name: any,
        // description: any
    };
    text: any;
    productInfo: any;
    constructor(public navCtrl: NavController, public navParams: NavParams,
        public menu: MenuController, public formBuilder: FormBuilder,
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController, public login: LoginService,
        public ionic: IonicUtilProvider, public common: CommonService) {
        this.productInfo = this.navParams.get('product');
        console.log(this.productInfo);
        //let regex = '[a-zA-Z0-9]+(,[a-zA-Z0-9]+)*';
        let regex = '[a-zA-Z0-9,-]*$';
        if (this.productInfo == undefined) {
            this.addProductForm = this.formBuilder.group({
                id: "",
                design_number: ['', Validators.compose([Validators.required,Validators.pattern(regex)])]
                // product_name: ['', Validators.compose([Validators.required])],
                // description: ['', Validators.compose([Validators.maxLength(500)])]
            });
        }
        else {
            this.addProductForm = this.formBuilder.group({
                id: this.productInfo.id,
                design_number: [this.productInfo.design_number, Validators.compose([Validators.required, CustomValidators.digits])]
                // product_name: [this.productInfo.product_name, Validators.compose([Validators.required])],
                // description: [this.productInfo.description, Validators.compose([Validators.maxLength(500)])]
            });
        }

    }

    ionViewDidLoad() {
        this.menu.close();
        this.ionic.loadingShow();
    }

    addProduct() {
        this.ionic.loadingShow();
        if (this.addProductForm.valid) {
        
            this.data = {
                id: this.addProductForm.value.id,
                design_number: this.addProductForm.value.design_number
                // product_name: this.addProductForm.value.product_name,
                // description: this.addProductForm.value.description
            }
            console.log(this.data);
            this.common.addProduct(this.data).subscribe(res => {
                if (res['status'] == 1) {
                    this.ionic.showToastAlert(res['message']);
                    this.navCtrl.setRoot(ProductsPage);
                    this.ionic.loadingHide();
                }
                else {
                    this.ionic.showToastAlert(res['message']);
                }
            },
            error => {
                this.ionic.showToastAlert("Error in add product");
                this.ionic.loadingHide();
            });
        }
        else {
            this.ionic.showToastAlert("Invalid Form details");
            this.ionic.loadingHide();
        }
    }

    home() {
        this.navCtrl.setRoot(HomePage);
    }
}
