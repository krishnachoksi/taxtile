import { CommonService } from './../../app/core/service/common.service';
import { Component } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { NavController, NavParams, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../../app/core/service/login.service';
import { IonicUtilProvider } from '../../providers/ionic-util/ionic-util';
import { Utility } from '../../app/core/utility';
import { ChangePasswordPage } from '../change-password/change-password';
import { LoginPage } from '../login/login';

@Component({
	selector: 'page-edit-profile',
	templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
	loading: any;
	createSuccess = false;
	userData: any;
	public editUserForm;
	data: { name: any, email: any, company_name: any };

	constructor(public navCtrl: NavController, public navParams: NavParams,
		public formBuilder: FormBuilder, public loadingCtrl: LoadingController,
		public alertCtrl: AlertController, public login: LoginService,
		public ionic: IonicUtilProvider,
		public common: CommonService, public menu: MenuController) {
		this.userData = JSON.parse(localStorage.getItem('.user_data'));
		console.log(this.userData);
		if (this.userData == null) {
			this.navCtrl.setRoot(LoginPage);
		}

		this.editUserForm = this.formBuilder.group({
			name: [this.userData.name, Validators.compose([Validators.required])],
			email: [this.userData.email, Validators.compose([Validators.required, CustomValidators.email])],
			company_name: [this.userData.company_name, Validators.compose([Validators.required])]
		});
	}

	ionViewDidLoad() {
		this.menu.close();
	}

	ionViewDidEnter() {
		this.menu.close();
	}

	home() {
		this.navCtrl.setRoot(HomePage);
	}

	editUser() {
		console.log(this.editUserForm.value);
		this.editUserForm.value.id = this.userData.id;
		this.common.editProfile(this.editUserForm.value).subscribe(res => {
			console.log(res);
			if (res['status'] == "1") {
				localStorage.removeItem('.user_data');
				Utility.setLS('user_data', JSON.stringify(res['data']['userDetail']));
				this.ionic.showToastAlert(res['message']);
				this.navCtrl.setRoot(HomePage);
				this.ionic.loadingHide();
			}
			else {
				this.ionic.showToastAlert(res['message']);
				console.log("error");
			}
		},
			error => {
				console.log(error.error['message']);
				this.ionic.showToastAlert(error.error['message']);
			});
	}

	goToChangePassword() {
		this.navCtrl.push(ChangePasswordPage);
	}
}
