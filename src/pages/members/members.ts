import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { HomePage } from '../../pages/home/home';
import { LoginService } from '../../app/core/service/login.service';
import { IonicUtilProvider } from '../../providers/ionic-util/ionic-util';
import { CommonService } from './../../app/core/service/common.service';
import { AddMemberPage } from '../add-member/add-member';

@Component({
	selector: 'page-members',
	templateUrl: 'members.html',
})
export class MembersPage {
	data: { id: any };
	memberData: any;
	userData: any;
	token: any;
	display = false;
	selectedAll = false;
	delete = [];
	select = false;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public loadingCtrl: LoadingController, public alertCtrl: AlertController,
		public login: LoginService, public ionic: IonicUtilProvider,
		public common: CommonService) {

		this.userData = JSON.parse(localStorage.getItem('.user_data'));
		this.token = JSON.parse(localStorage.getItem('.token'));

		if (this.userData != null) {
			this.getMembers();
		}
		else {
			this.navCtrl.setRoot(LoginPage);
		}
	}

	ionViewDidLoad() { }

	ionViewDidEnter() {
		this.getMembers();
	}

	home() {
		this.navCtrl.setRoot(HomePage);
	}

	getMembers() {
		this.common.getAllMembers().subscribe(res => {
			if (res['status'] == 1) {
				this.memberData = res['data'];
			}
			else {
				this.ionic.showToastAlert(res['message']);
				console.log("error");
			}
		},
			error => {
				console.log("errorrr");
				this.ionic.showToastAlert("Can not able to login.");
				this.ionic.loadingHide();
			});
	}

	openAddMember() {
		this.navCtrl.push(AddMemberPage);
	}

	editMember(event, data) {
		event.stopPropagation();
		this.navCtrl.push(AddMemberPage, { member: data });
	}

	checkAll(event){
		if(event.checked){
			this.selectedAll = true;
			this.display = true;
		}
		else{
			this.selectedAll = false;
			this.display = false;
		}
		if(this.selectedAll == true){
			for(let x in this.memberData){
				this.delete.push(this.memberData[x].id);
			}
			//this.deleteMem(this.delete);
		}
		else{
			this.delete = [];
		}
		console.log(this.delete);
	}

	deleteMember(event,index,id) {
		//this.delete = [];
		if(event.target.checked){
			this.delete.push(id);
			this.select = (this.delete.length == this.memberData.length)?true:false;
		}
		else{
			var i = this.delete.indexOf(id);
			this.delete.splice(i, 1);
			this.select = (this.delete.length == this.memberData.length)?true:false;
		}
		this.display = (this.delete.length > 0)?true:false;
		console.log(this.delete);
		//this.deleteMem(this.delete);
		
	}

	deleteMem(data){
		let alert = this.alertCtrl.create({
			title: 'Confirm Delete',
			message: 'Do you want to delete this member?It will delete all orders of this member too.!!',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
					}
				},
				{
					text: 'Delete',
					handler: () => {
						this.ionic.loadingShow();
						this.data = { id:data };
						this.common.deleteMember(this.data).subscribe(res => {
							console.log(res);
							if (res['status'] == 1) {
								this.ionic.showToastAlert(res['message']);
								this.display = false;
								this.getMembers();
								this.ionic.loadingHide();
							}
						},
							error => {
								this.ionic.showToastAlert("Error in getting member list.");
								this.ionic.loadingHide();
							});
					}
				}
			]
		});
		alert.present();
	}
}
