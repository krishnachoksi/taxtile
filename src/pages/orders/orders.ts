import { Component,QueryList,ViewChildren } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController,ModalController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { HomePage } from '../../pages/home/home';
import { LoginService } from '../../app/core/service/login.service';
import { IonicUtilProvider } from '../../providers/ionic-util/ionic-util';
import { CommonService } from './../../app/core/service/common.service';
import { AddOrderPage } from '../add-order/add-order';
import { Utility } from '../../app/core/utility';
import { reorderArray } from 'ionic-angular';
import { ModalPage } from '../modal/modal';

@Component({
	selector: 'page-orders',
	templateUrl: 'orders.html',
})
export class OrdersPage {
	@ViewChildren('allTheseThings')things: QueryList<any>;
	id: any;
	orderData: any;
	userData: any;
	token: any;
	isOpened: boolean = false;
	show = false;
	text: any;
	statusData: { order_id: any, status: any };
	showToggle: boolean = true;
	status: any;
	order: any;
	showButton = true;
	display: any;
	pending: any;
	orderSegment: any;
	data: { quantity: any, order_id: any };
	quantityData: any;
	count = 0;
	digital_pending: any;
	digital_start: any;
	polishing_pending: any;
	polishing_start: any;
	completed: any;
	approvedOrder: any;
	rejectedOrder: any;
	pendingOrder: any;
	polishing_order: any;
	digitalPending: any;
	digitalStart: any;
	digitalCompleted: any;
	polishingPending: any;
	polishingStart: any;
	polishingCompleted: any;
	deleteData : {id: any};
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public loadingCtrl: LoadingController, public alertCtrl: AlertController,
		public login: LoginService, public ionic: IonicUtilProvider,
		public common: CommonService,public modal:ModalController) {

		this.userData = JSON.parse(localStorage.getItem('.user_data'));
		this.token = JSON.parse(localStorage.getItem('.token'));
		this.orderSegment = (this.userData.user_type === 'admin' || this.userData.user_type === 'marketing')?"all":"running";
		if (this.userData != null) {
			this.getAllOrders();
		}
		else {
			this.navCtrl.setRoot(LoginPage);
		}
	}

	reorderItem(index): void {

		// Reset the items array
		//this.orderData = [];
		this.approvedOrder = reorderArray(this.approvedOrder, index);
		var finalArray = this.approvedOrder.map(function (obj) {
			return obj.id;
		});
	
		this.common.swapOrder(finalArray).subscribe(res => {
			console.log(res);
			if (res['status'] == 1) {
				this.ionic.showToastAlert(res['message']);
				this.getAllOrders();
			} else {
				this.ionic.showToastAlert(res['message']);
			}
		},
		error => {
			this.ionic.showToastAlert(error['error'].message);
			this.ionic.loadingHide();
		})
	}

	ionViewDidLoad() {
		this.ionic.loadingShow();
	}

	startPause(id) {
		console.log(id);
		console.log(this.orderData);
		for (let i = 0; i < this.orderData.length; i++) {
			if (this.orderData[i].id == id) {
				this.order = this.orderData[i];
			}
		}
		console.log(this.order);
		if (this.userData.user_type === 'digitalmachine') {
			this.text = (this.order.digital_status == 2) ? "Pause" : "Start";
			this.status = (this.order.digital_status == 2) ? 3 : 2;
		}
		else {
			this.text = (this.order.polishing_status == 2) ? "Pause" : "Start";
			this.status = (this.order.polishing_status == 2) ? 3 : 2;
		}

		if (this.status == 3) {
			const prompt = this.alertCtrl.create({
				title: 'Pause',
				message: "Please Enter Completed Quantity.",
				inputs: [
					{
						name: 'quantity',
						placeholder: 'Quantity',
						type: 'number',
						value: this.order.quantity
					},
				],
				buttons: [
					{
						text: 'Cancel',
						handler: data => {

						}
					},
					{
						text: 'Save',
						handler: data => {
							console.log(Number(this.order.quantity));
							let validateObj = this.validateQuantity(data.quantity, Number(this.order.quantity));
							if (!validateObj) {
								this.ionic.showToastAlert('Please enter valid quantity');
							} else {
								this.completedQuantity(data.quantity, this.order.id);
							}
						}
					}
				]
			});
			prompt.present();
		}
		else {
			this.statusData = {
				order_id: this.order.id,
				status: this.status
			}
			const confirm = this.alertCtrl.create({
				title: 'Start Or Pause?',
				message: 'Do you really want to ' + this.text + ' this order?This will complete previous order',
				buttons: [
					{
						text: this.text,
						handler: () => {

							this.common.changeOrderStatus(this.statusData).subscribe(res => {

								if (res['status'] == 1) {
									this.ionic.showToastAlert(res['message']);
									this.getAllOrders();
								}
								else {
									this.ionic.showToastAlert(res['message']);
									console.log("error");
								}
							},
								error => {
									this.ionic.showToastAlert(error['error'].message);
									this.ionic.loadingHide();
								});
						}
					},
					{
						text: 'Cancel',
						handler: () => {

						}
					}
				]
			});
			confirm.present();
		}
	}
	validateQuantity(quantity, old_quantity) {
		if (quantity > old_quantity) {
			return false;
		}
		else {
			return true;
		}
	}
	getAllOrders() {
		this.id = this.userData['id'];
		if (this.userData.user_type === 'admin') {
			this.common.getAllOrders().subscribe(res => {
				if (res['status'] == 1) {
					this.orderData = res['data'];
					this.digital_pending = res['digitalPending'];
					this.digital_start = res['digitalStartPause'];
					this.polishing_pending = res['polishingPending'];
					this.polishing_start = res['polishingStartPause'];
					this.completed = res['completed'];
					this.pendingOrder = res['getPendingOrderData'];
					this.rejectedOrder = res['getRejectOrderData'];
					this.approvedOrder = res['getApprovedOrderData'];
					this.polishing_order = res['getPolishingOrderData'];
					for(let i = 0; i < this.polishing_order.length;i++){
						if(this.polishing_order[i].polishing_completed_quantity != null && this.polishing_order[i].polishing_status == 5){
							this.polishing_order[i].quantity = this.polishing_order[i].polishing_completed_quantity;
						}
						if(this.polishing_order[i].completed_quantity != null && this.polishing_order[i].digital_status == 5 && this.polishing_order[i].polishing_status !== 5 && this.polishing_order[i].polishing_completed_quantity == null){
							this.polishing_order[i].quantity = this.polishing_order[i].completed_quantity;
						}
						if(this.polishing_order[i].completed_quantity != null && this.polishing_order[i].digital_status == 5 && this.polishing_order[i].polishing_status === 5 && this.polishing_order[i].polishing_completed_quantity == null){
							this.polishing_order[i].quantity = this.polishing_order[i].completed_quantity;
						}
						if (this.polishing_order[i].polishing_status === 1) {
							this.polishing_order[i].polishing_pending = true;
						}

						if (this.polishing_order[i].polishing_status === 2 || this.polishing_order[i].polishing_status === 3 ) {
							this.polishing_order[i].polishing_display = true;
						}
					}
					for (let i = 0; i < this.orderData.length; i++) {
						if(this.orderData[i].completed_quantity != null && this.orderData[i].completed_quantity != this.orderData[i].quantity){
							this.orderData[i].quantity = (this.orderData[i].quantity - this.orderData[i].completed_quantity);
						}
						if(this.orderData[i].completed_quantity === this.orderData[i].quantity){
							this.orderData[i].quantity = this.orderData[i].quantity;
						}
				
						if (this.orderData[i].digital_status == 1) {
							this.orderData[i].showToggle = true;
						}
						else {
							this.orderData[i].showToggle = false;
						}
						if (this.orderData[i].digital_status == 5 && this.orderData[i].polishing_status == 5) {
							this.orderData[i].showButton = false;
							this.orderData[i].completed = true;
						}
						if (this.orderData[i].digital_status === 1 && this.orderData[i].admin_status == 2) {
							this.orderData[i].digital_pending = true;
						}
				
						if (this.orderData[i].digital_status === 2 || this.orderData[i].digital_status === 3 ) {
							this.orderData[i].digital_display = true;
						}
						
						if (this.orderData[i].company_name == null && this.userData.user_type == 'admin') {
							this.orderData[i].company_name = 'Admin';
						}
					}
				}
				else {
					this.ionic.showToastAlert(res['message']);
					console.log("error");
				}
			},
				error => {
					console.log("errorrr");
					this.ionic.showToastAlert(error['error'].message);
					this.ionic.loadingHide();
				});
		} else if (this.userData.user_type === 'marketing') {
			this.common.getAllOrders().subscribe(res => {
				if (res['status'] == 1) {
					this.orderData = res['data'];
					this.digital_pending = res['digitalPending'];
					this.digital_start = res['digitalStartPause'];
					this.polishing_pending = res['polishingPending'];
					this.polishing_start = res['polishingStartPause'];
					this.completed = res['completed'];
					this.approvedOrder = res['getApprovedOrderData'];
					this.pendingOrder = res['getPendingOrderData'];
					this.rejectedOrder = res['getRejectOrderData'];
					this.polishing_order = res['getPolishingOrderData'];
					for(let i = 0; i < this.polishing_order.length;i++){
						
						if(this.polishing_order[i].polishing_completed_quantity != null && this.polishing_order[i].polishing_status == 5){
							this.polishing_order[i].quantity = this.polishing_order[i].polishing_completed_quantity;
						}
						if(this.polishing_order[i].completed_quantity != null && this.polishing_order[i].digital_status == 5 && this.polishing_order[i].polishing_status !== 5 && this.polishing_order[i].polishing_completed_quantity == null){
							this.polishing_order[i].quantity = this.polishing_order[i].completed_quantity;
						}
						if(this.polishing_order[i].completed_quantity != null && this.polishing_order[i].digital_status == 5 && this.polishing_order[i].polishing_status === 5 && this.polishing_order[i].polishing_completed_quantity == null){
							this.polishing_order[i].quantity = this.polishing_order[i].completed_quantity;
						}
						if (this.polishing_order[i].polishing_status === 1) {
							this.polishing_order[i].polishing_pending = true;
						}

						if (this.polishing_order[i].polishing_status === 2 || this.polishing_order[i].polishing_status === 3 ) {
							this.polishing_order[i].polishing_display = true;
						}
					}
					for (let i = 0; i < this.orderData.length; i++) {
						if(this.orderData[i].completed_quantity != null && this.orderData[i].completed_quantity != this.orderData[i].quantity){
							this.orderData[i].quantity = (this.orderData[i].quantity - this.orderData[i].completed_quantity);
						}
						if(this.orderData[i].completed_quantity === this.orderData[i].quantity){
							this.orderData[i].quantity = this.orderData[i].quantity;
						}
						if (this.orderData[i].digital_status == 1) {
							this.orderData[i].showToggle = true;
						}
						else {
							this.orderData[i].showToggle = false;
						}
						if (this.orderData[i].digital_status == 5 && this.orderData[i].polishing_status == 5) {
							this.orderData[i].showButton = false;
							this.orderData[i].completed = true;
						}
						if (this.orderData[i].digital_status === 1 && this.orderData[i].admin_status == 2) {
							this.orderData[i].digital_pending = true;
						}
						// if (this.orderData[i].polishing_status === 1 && this.orderData[i].admin_status == 2 && this.orderData[i].digital_status == 5) {
						// 	this.orderData[i].polishing_pending = true;
						// }
						if (this.orderData[i].digital_status === 2 || this.orderData[i].digital_status === 3 ) {
							this.orderData[i].digital_display = true;
						}
						// if (this.orderData[i].polishing_status === 2 || this.orderData[i].polishing_status === 3 ) {
						// 	this.orderData[i].polishing_display = true;
						// }
						if (this.orderData[i].company_name == null && this.userData.user_type == 'admin') {
							this.orderData[i].company_name = 'Admin';
						}
					}
				}
				else {
					this.ionic.showToastAlert(res['message']);
				}
			},
				error => {
					this.ionic.showToastAlert("Error in getting order list.");
					this.ionic.loadingHide();
				});
		}
		else if (this.userData.user_type === 'digitalmachine') {
			this.common.getOrderByUserId(this.id).subscribe(res => {
				console.log(res);
				if (res['status'] == 1) {
					this.orderData = res['data']['all'];
					this.digitalPending = res['data']['digital_pending'];
					this.digital_pending = res['digitalPending'];
					this.digital_start = res['digitalStartPause'];
					this.polishing_pending = res['polishingPending'];
					this.polishing_start = res['polishingStartPause'];
					this.completed = res['completed'];
					this.digitalStart = res['data']['digital_start'];
					this.digitalCompleted = res['data']['digital_completed'];
		
					for (let i = 0; i < this.digitalPending.length; i++) {
						this.digitalPending[i].showButton = true;
						if(this.digitalPending[i].completed_quantity != null && this.digitalPending[i].digital_status == 5){
							this.digitalPending[i].quantity = this.digitalPending[i].completed_quantity;
						}
						if (this.digitalPending[i].company_name == null && this.userData.user_type == 'admin') {
							this.digitalPending[i].company_name = 'Admin';
						}
					}
					for (let i = 0; i < this.digitalStart.length; i++) {
						this.digitalStart[i].showButton = true;
						if(this.digitalStart[i].completed_quantity != null && this.digitalStart[i].digital_status == 5){
							this.digitalStart[i].quantity = this.digitalStart[i].completed_quantity;
						}
						if (this.digitalStart[i].company_name == null && this.userData.user_type == 'admin') {
							this.digitalStart[i].company_name = 'Admin';
						}
					}

					for (let i = 0; i < this.digitalCompleted.length; i++) {
						if(this.digitalCompleted[i].completed_quantity != null && this.digitalCompleted[i].digital_status == 5){
							this.digitalCompleted[i].quantity = this.digitalCompleted[i].completed_quantity;
						}
						if (this.digitalCompleted[i].company_name == null && this.userData.user_type == 'admin') {
							this.digitalCompleted[i].company_name = 'Admin';
						}
					}
				}
				else {
					this.ionic.showToastAlert(res['message']);
				}
			},
				error => {
					this.ionic.showToastAlert("Error in getting order list.");
					this.ionic.loadingHide();
				});
		}
		else if (this.userData.user_type === 'polishingmachine') {
			this.common.getOrderByUserId(this.id).subscribe(res => {
				console.log(res);
				if (res['status'] == 1) {
					this.orderData = res['data']['all'];
					this.digital_pending = res['digitalPending'];
					this.digital_start = res['digitalStartPause'];
					this.polishing_pending = res['polishingPending'];
					this.polishing_start = res['polishingStartPause'];
					this.completed = res['completed'];
					this.polishingPending = res['data']['polishing_pending'];
					this.polishingStart = res['data']['polishing_start'];
					this.polishingCompleted = res['data']['polishing_completed'];
					for (let i = 0; i < this.polishingPending.length; i++) {
						this.orderData[i].showButton = true;
						if(this.polishingPending[i].polishing_completed_quantity != null && this.polishingPending[i].polishing_status == 5){
							this.polishingPending[i].quantity = this.polishingPending[i].polishing_completed_quantity;
						}
						if(this.polishingPending[i].completed_quantity != null && this.polishingPending[i].digital_status == 5 && this.polishingPending[i].polishing_status !== 5 && this.polishingPending[i].polishing_completed_quantity == null){
							this.polishingPending[i].quantity = this.polishingPending[i].completed_quantity;
						}
						if(this.polishingPending[i].completed_quantity != null && this.polishingPending[i].digital_status == 5 && this.polishingPending[i].polishing_status === 5 && this.polishingPending[i].polishing_completed_quantity == null){
							this.polishingPending[i].quantity = this.polishingPending[i].completed_quantity;
						}
						if (this.polishingPending[i].company_name == null && this.userData.user_type == 'admin') {
							this.polishingPending[i].company_name = 'Admin';
						}
					}
					for (let i = 0; i < this.polishingStart.length; i++) {
						if(this.polishingStart[i].polishing_completed_quantity != null && this.polishingStart[i].polishing_status == 5){
							this.polishingStart[i].quantity = this.polishingStart[i].polishing_completed_quantity;
						}
						if(this.polishingStart[i].completed_quantity != null && this.polishingStart[i].digital_status == 5 && this.polishingStart[i].polishing_status !== 5 && this.polishingStart[i].polishing_completed_quantity == null){
							this.polishingStart[i].quantity = this.polishingStart[i].completed_quantity;
						}
						if(this.polishingStart[i].completed_quantity != null && this.polishingStart[i].digital_status == 5 && this.polishingStart[i].polishing_status === 5 && this.polishingStart[i].polishing_completed_quantity == null){
							this.polishingStart[i].quantity = this.polishingStart[i].completed_quantity;
						}
						if (this.polishingStart[i].company_name == null && this.userData.user_type == 'admin') {
							this.polishingStart[i].company_name = 'Admin';
						}
					}
					for (let i = 0; i < this.polishingCompleted.length; i++) {
						if(this.polishingCompleted[i].polishing_completed_quantity != null && this.polishingCompleted[i].polishing_status == 5){
							this.polishingCompleted[i].quantity = this.polishingCompleted[i].polishing_completed_quantity;
						}
						if(this.polishingCompleted[i].completed_quantity != null && this.polishingCompleted[i].digital_status == 5 && this.polishingCompleted[i].polishing_status !== 5 && this.polishingCompleted[i].polishing_completed_quantity == null){
							this.polishingCompleted[i].quantity = this.polishingCompleted[i].completed_quantity;
						}
						if(this.polishingCompleted[i].completed_quantity != null && this.polishingCompleted[i].digital_status == 5 && this.polishingCompleted[i].polishing_status === 5 && this.polishingCompleted[i].polishing_completed_quantity == null){
							this.polishingCompleted[i].quantity = this.polishingCompleted[i].completed_quantity;
						}
						if (this.polishingCompleted[i].company_name == null && this.userData.user_type == 'admin') {
							this.polishingCompleted[i].company_name = 'Admin';
						}
					}
				}
				else {
					this.ionic.showToastAlert(res['message']);
				}
			},
				error => {
					this.ionic.showToastAlert("Error in getting order list.");
					this.ionic.loadingHide();
				});
		}
	}

	home() {
		this.navCtrl.setRoot(HomePage);
	}

	openAddOrder() {
		this.navCtrl.push(AddOrderPage);
	}

	completedQuantity(quantity, id) {
		console.log(id);
		this.data = { quantity: quantity, order_id: id };
		this.common.completedQuantity(this.data).subscribe(res => {
			console.log(res);
			if (res['status'] == 1) {
				this.ionic.showToastAlert(res['message']);
				this.getAllOrders();
			}
			else {
				this.ionic.showToastAlert(res['message']);
				console.log("error");
			}
		},
			error => {
				console.log(error.error['message']);
				this.ionic.showToastAlert(error.error['message']);
				this.ionic.loadingHide();
			});

	}

	changeStatus(data) {
		console.log(data);
		this.text = (data.admin_status == 2) ? "Reject" : "Approve";
	
		const confirm = this.alertCtrl.create({
			title: 'Approve Or Reject?',
			//message: 'Approve or ',
			buttons: [
				{
					text: 'Approve',
					handler: () => {
						this.statusData = {
							order_id: data.id,
							status: 2
						}
						console.log(this.statusData);
						this.common.changeOrderStatus(this.statusData).subscribe(res => {
							if (res['status'] == 1) {
								this.ionic.showToastAlert(res['message']);
								this.getAllOrders();
							}
							else {
								this.ionic.showToastAlert(res['message']);
								console.log("error");
							}
						},
						error => {
							this.ionic.showToastAlert(error['error'].message);
							this.ionic.loadingHide();
						});
					}
				},
				{
					text: 'Reject',
					handler: () => {
						this.statusData = {
							order_id: data.id,
							status: 3
						}
						console.log(this.statusData);
						this.common.changeOrderStatus(this.statusData).subscribe(res => {
							if (res['status'] == 1) {
								this.ionic.showToastAlert(res['message']);
								this.getAllOrders();
							}
							else {
								this.ionic.showToastAlert(res['message']);
								console.log("error");
							}
						},
						error => {
							this.ionic.showToastAlert(error['error'].message);
							this.ionic.loadingHide();
						});
					}
				}
			]
		});
		confirm.present();
	}

	logout() {
		Utility.removeLS('user_data');
		Utility.removeLS('token');
		Utility.removeLS('loginValue');
		Utility.removeLS('password');
		this.navCtrl.setRoot(LoginPage);
	}
	
	polishingOrder(index){
		console.log(index);
		this.polishing_order = reorderArray(this.polishing_order, index);
		var finalArray = this.polishing_order.map(function (obj) {
			return obj.id;
		});
		console.log(this.polishing_order);
		this.common.swapPolishingOrder(finalArray).subscribe(res => {
			console.log(res);
			if (res['status'] == 1) {
				this.ionic.showToastAlert(res['message']);
				this.getAllOrders();
			} else {
				this.ionic.showToastAlert(res['message']);
			}
		},
		error => {
			this.ionic.showToastAlert(error['error'].message);
			this.ionic.loadingHide();
		})
	}

	viewTime(item){
		console.log(item);
		let modal = this.modal.create(ModalPage,item);
    	modal.present();
	}

	deleteRejected(id){
		const confirm = this.alertCtrl.create({
			title: 'Are you sure you want to delete this order?',
			buttons: [
				{
					text: 'Delete',
					handler: () => {
						this.deleteData = {id: id};
						this.common.deleteOrder(this.deleteData).subscribe(res => {
							console.log(res);
							if (res['status'] == 1) {
								this.ionic.showToastAlert(res['message']);
								this.getAllOrders();
							} else {
								this.ionic.showToastAlert(res['message']);
							}
						},
						error => {
							this.ionic.showToastAlert(error['error'].message);
							this.ionic.loadingHide();
						})
					}
				},
				{
					text: 'Cancel',
					handler: () => {
						
					}
				}
			]
		});
		confirm.present();

	}

}
