import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CustomValidators } from 'ng2-validation';
import { FormBuilder, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { LoadingController, Loading } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { CommonService } from './../../app/core/service/common.service';
import { IonicUtilProvider } from '../../providers/ionic-util/ionic-util';
import { HomePage } from '../home/home';

function NotSameAsOld(oldPwd: AbstractControl): ValidatorFn {
	// const hasExclamation = input.value !== this.o_password.value;
	return (control: AbstractControl) => {
		return control.value === oldPwd.value ? {
			NotSameAsOld: {
				valid: false
			}
		} : null;
	};
}

const oldPassword = new FormControl(null, Validators.compose([Validators.required, Validators.minLength(8)]));
const newPassword = new FormControl(null, {
	validators: Validators.compose([Validators.required,
	Validators.minLength(8), NotSameAsOld(oldPassword)])
});
const confirmPassword = new FormControl(null, {
	validators: Validators.compose([Validators.required, CustomValidators.equalTo(newPassword)])
});

@Component({
	selector: 'page-change-password',
	templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

	userData: any;
	public changePasswordForm;
	loading: Loading;
	createSuccess = false;
	validation_messages: any;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public formBuilder: FormBuilder,
		public loadingCtrl: LoadingController, public common: CommonService,
		public ionic: IonicUtilProvider) {
		this.userData = JSON.parse(localStorage.getItem('.user_data'));
		if (this.userData == null) {
			this.navCtrl.setRoot(LoginPage);
		}

		this.changePasswordForm = formBuilder.group({
			old_password: oldPassword,
			new_password: newPassword,
			password_confirmation: confirmPassword
		});

		this.validation_messages = {
			'old_password': [
				{ type: 'required', message: 'Old Password is required.' },
				{ type: 'minlength', message: 'Password must be at least 8 characters long.' }
			],
			'new_password': [
				{ type: 'required', message: 'New Password is required.' },
				{ type: 'minlength', message: 'Password must be at least 8 characters long.' },
				{ type: 'NotSameAsOld', message: 'New password can not be same as old password.' }
			],
			'password_confirmation': [
				{ type: 'required', message: 'Confirm Password is required.' },
				{ type: 'equalTo', message: 'Please Enter same password' }
			]
		}
	}

	ionViewDidLoad() { }

	changePassword() {
		console.log(this.changePasswordForm.value);
		this.common.changePassword(this.changePasswordForm.value).subscribe(res => {
			console.log(res);
			if (res['status'] == "1") {
				this.ionic.showToastAlert(res['message']);
				this.logout();
			}
			else {
				this.ionic.showToastAlert(res['message']);
				console.log("error");
			}
		},
			error => {
				console.log(error.error['message']);
				this.ionic.showToastAlert(error.error['message']);
			});
	}

	logout() {
		localStorage.removeItem('.user_data');
		localStorage.removeItem('.token');
		localStorage.removeItem('.password');
		localStorage.removeItem('.loginValue');
		this.navCtrl.setRoot(LoginPage);
	}

	home() {
		this.navCtrl.setRoot(HomePage);
	}
}
