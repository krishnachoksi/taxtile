import { Component } from '@angular/core';
import { NavController, NavParams,ViewController } from 'ionic-angular';

@Component({
	selector: 'page-modal',
	templateUrl: 'modal.html',
})
export class ModalPage {

	data: any;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public view: ViewController) {
		this.data = this.navParams.data;
		console.log(this.data);
	}

	ionViewDidLoad() {

	}

	close(){
		this.view.dismiss();
	}

}
