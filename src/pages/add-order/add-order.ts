import { Component } from '@angular/core';
import { CommonService } from './../../app/core/service/common.service';
import { NavController, NavParams, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { OrdersPage } from '../../pages/orders/orders';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../../app/core/service/login.service';
import { IonicUtilProvider } from '../../providers/ionic-util/ionic-util';
import { HomePage } from '../home/home';
import { CustomValidators } from 'ng2-validation';
import { LoginPage } from '../login/login';

@Component({
    selector: 'page-add-order',
    templateUrl: 'add-order.html',
})
export class AddOrderPage {
    id: any;
    loading: any;
    createSuccess = false;
    userData: any;
    public addOrderForm;
    productData: any;
    token: any;
    design_number: any;
    data: { design_number: any, quantity: any,  note: any };
    text: any;
    constructor(public navCtrl: NavController, public navParams: NavParams,
        public formBuilder: FormBuilder, public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public login: LoginService, public ionic: IonicUtilProvider,
        public common: CommonService, public menu: MenuController) {
       
        let regex = '^[1-9][0-9]*$';
        this.addOrderForm = this.formBuilder.group({
            design_number: ['', Validators.compose([Validators.required])],
            quantity: ['', Validators.compose([Validators.required,Validators.pattern(regex), CustomValidators.digits,CustomValidators.gt(0),Validators.maxLength(6)])],
            //priority: ['', Validators.compose([Validators.required])],
            note: ['', Validators.compose([Validators.maxLength(500)])]
        });

        this.userData = JSON.parse(localStorage.getItem('.user_data'));
        this.token = JSON.parse(localStorage.getItem('.token'));

        if (this.userData != null) {
            console.log(this.userData['user_type']);
            this.getAllProducts();
        }
        else {
            this.navCtrl.setRoot(LoginPage);
        }
    }

    ionViewDidLoad() {
        this.menu.close();
        this.ionic.loadingShow();
    }

    home() {
        this.navCtrl.setRoot(HomePage);
    }

    addOrder() {
        this.ionic.loadingShow();
        if (this.addOrderForm.valid) {
            this.addOrderForm.value.token = JSON.parse(localStorage.getItem('.token'));
            this.data = {
                design_number: this.addOrderForm.value.design_number.design_number,
                quantity: this.addOrderForm.value.quantity,
                note: this.addOrderForm.value.note
            }
            //console.log(this.data);
            this.common.addOrder(this.data).subscribe(res => {
                if (res['status'] == 1) {
                    this.ionic.showToastAlert(res['message']);
                    this.navCtrl.setRoot(OrdersPage);
                    this.ionic.loadingHide();
                }
                else {
                    this.ionic.showToastAlert(res['message']);
                }
            },
                error => {
                    this.ionic.showToastAlert("Error in adding order");
                    this.ionic.loadingHide();
                });
        }
        else {
            this.ionic.showToastAlert("Invalid Form details");
            this.ionic.loadingHide();
        }
    }

    getAllProducts() {
        this.id = this.userData['id'];
        this.common.getAllProducts().subscribe(res => {
            if (res['status'] == 1) {
                this.productData = res['data'];
                console.log(this.productData);
            }
            else {
                this.ionic.showToastAlert(res['message']);
                console.log("error");
            }
        },
            error => {
                console.log("errorrr");
                this.ionic.showToastAlert("Can not able to login.");
                this.ionic.loadingHide();
            });

    }
}
