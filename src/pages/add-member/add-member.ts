import { CommonService } from './../../app/core/service/common.service';
import { Component } from '@angular/core';
import { CustomValidators } from 'ng2-validation';
import { NavController, NavParams, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../../app/core/service/login.service';
import { IonicUtilProvider } from '../../providers/ionic-util/ionic-util';
import { MembersPage } from '../members/members';
import { LoginPage } from '../login/login';

@Component({
    selector: 'page-add-member',
    templateUrl: 'add-member.html',
})
export class AddMemberPage {
    loading: any;
    createSuccess = false;
    userData: any;
    public addUserForm;
    data: { id:any,name: any, password: any, user_type: any };
    text: any;
    memberInfo: any;
    constructor(public navCtrl: NavController, public navParams: NavParams,
        public formBuilder: FormBuilder, public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,public login: LoginService, 
        public ionic: IonicUtilProvider,
        public common: CommonService, public menu: MenuController) {
        
        this.userData = JSON.parse(localStorage.getItem('.user_data'));
        console.log(this.userData);
        if(this.userData == null){
            this.navCtrl.setRoot(LoginPage);
        }

        this.memberInfo = this.navParams.get('member');
        if(this.memberInfo == undefined) {
            this.addUserForm = this.formBuilder.group({
                id: "",
                name: ['', Validators.compose([Validators.required])],
                //email: ['', Validators.compose([Validators.required, CustomValidators.email])],
                password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
                user_type: ['', Validators.compose([Validators.required])]
                //company_name: ['', Validators.compose([Validators.required])]
            });
        }
        else{
            this.addUserForm = this.formBuilder.group({
                id: [this.memberInfo.id],
                name: [this.memberInfo.name, Validators.compose([Validators.required])],
               // email: [this.memberInfo.email, Validators.compose([Validators.required, CustomValidators.email])],
                password: ['', Validators.compose([Validators.minLength(6)])],
                user_type: [this.memberInfo.user_type, Validators.compose([Validators.required])]
                //company_name: [this.memberInfo.company_name, Validators.compose([Validators.required])]
            });
        }

    }

    ionViewDidLoad() {
        this.menu.close();
        this.ionic.loadingShow();
    }

    addUser() {
        this.ionic.loadingShow();
        if (this.addUserForm.valid) {
            console.log(this.addUserForm.value);
            this.addUserForm.value.token = JSON.parse(localStorage.getItem('.token'));
            this.data = {
                id:this.addUserForm.value.id,
                name: this.addUserForm.value.name,
                user_type: this.addUserForm.value.user_type,
                //email: this.addUserForm.value.email,
                password: this.addUserForm.value.password
                //company_name: this.addUserForm.value.company_name
            }
            this.common.addMember(this.data).subscribe(res => {
                if (res['status'] == 1) {
                    this.ionic.showToastAlert(res['message']);
                    this.navCtrl.setRoot(MembersPage);
                    this.ionic.loadingHide();
                }
                else {
                    this.ionic.showToastAlert(res['message']);
                }
            },
            error => {
                this.ionic.showToastAlert("Error in member add.");
                this.ionic.loadingHide();
            });

        }
        else {
            this.ionic.showToastAlert("Invalid Form details");
            this.ionic.loadingHide();
        }
    }

    home() {
        this.navCtrl.setRoot(HomePage);
    }
}
