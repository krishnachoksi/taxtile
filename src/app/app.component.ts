import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

import { LoginService } from '../app/core/service/login.service';

import { OrdersPage } from '../pages/orders/orders';

import { AddOrderPage } from '../pages/add-order/add-order';

import { ProductsPage } from '../pages/products/products';
import { AddProductPage } from '../pages/add-product/add-product';

import { MembersPage } from '../pages/members/members';
import { AddMemberPage } from '../pages/add-member/add-member';

import { Utility } from './../app/core/utility';

import { Events } from 'ionic-angular';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {

	@ViewChild(Nav) nav: Nav;

	rootPage: any = LoginPage;
	type: any;
	pages: Array<{ title: string, component: any, icon: string }>;
	userData: any;
	constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
		public events: Events, public login: LoginService) {
		this.initializeApp();

		events.subscribe('user:login', (user) => {
			this.userData = user;
			if (user != null) {
				if (user.user_type === "admin") {
					this.pages = [
						{ title: 'Home', component: HomePage, icon: 'home' },
						{ title: 'Orders', component: OrdersPage, icon: 'cart' },
						{ title: 'Products', component: ProductsPage, icon: 'cube' },
						{ title: 'Members', component: MembersPage, icon: 'person-add' }
					];
				}
				else if (user.user_type === "marketing") {
					this.pages = [
						{ title: 'Home', component: HomePage, icon: 'home' },
						{ title: 'Orders', component: OrdersPage, icon: 'cart' },
					];
				}
				else if (user.user_type === "digitalmachine") {
					this.pages = [
						{ title: 'Home', component: HomePage, icon: 'home' }
					];
				}
				else {
					this.pages = [
						{ title: 'Home', component: HomePage, icon: 'home' }
					];
				}

			}
			this.type = Utility.getUserType(this.userData['user_type']);
		});

	}

	initializeApp() {
		this.platform.ready().then(() => {
			this.statusBar.styleDefault();
			this.splashScreen.hide();
		});
	}

	openPage(event, page) {
		event.stopPropagation();
		this.nav.setRoot(page.component);
	}

	openAddPage(event, title) {
		event.stopPropagation();
		console.log("dkjdf");
		if (title == "Orders") {
			this.nav.setRoot(AddOrderPage);
		}
		if (title == "Products") {
			this.nav.setRoot(AddProductPage);
		}
		if (title == "Members") {
			this.nav.setRoot(AddMemberPage);
		}
	}
	logout() {
		Utility.removeLS('user_data');
		Utility.removeLS('token');
		Utility.removeLS('loginValue');
		Utility.removeLS('password');
		this.nav.setRoot(LoginPage);
	}
	
	edit() {
		this.nav.setRoot(EditProfilePage);
	}
}
