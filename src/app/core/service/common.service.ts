import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";

@Injectable()
export class CommonService {

    constructor(private baseService: BaseService) {
    }
    //add
    addMember(data: any) {
        return this.baseService.post(`${APIConstant.addMember}`, data);
    }

    addOrder(data: any){
        return this.baseService.post(`${APIConstant.addOrder}`, data);
    }

    addProduct(data: any){
        return this.baseService.post(`${APIConstant.addProduct}`, data);
    }

    sendMail(data: any){
        console.log(data);
        return this.baseService.post(`${APIConstant.sendMail}`, data);
    }
    //get
    getAllOrders() {
        return this.baseService.get(`${APIConstant.getAllOrders}`);
    }

    getAllProducts() {
        return this.baseService.get(`${APIConstant.getAllProducts}`);
    }

    getAllMembers() {
        return this.baseService.get(`${APIConstant.getAllMembers}`);
    }

    getOrderByUserId(data: any) {
        return this.baseService.get(`${APIConstant.getOrderByUserId}`+ data);
    }

    

    changeOrderStatus(data:any){
        return this.baseService.post(`${APIConstant.changeOrderStatus}`, data);
    }

    deleteMember(data:any){
        return this.baseService.post(`${APIConstant.deleteMember}`, data);
    }

    deleteProduct(data:any){
        return this.baseService.post(`${APIConstant.deleteProduct}`, data);
    }

    deleteOrder(data:any){
        return this.baseService.post(`${APIConstant.deleteOrder}`, data);
    }

    editProfile(data: any){
        return this.baseService.post(`${APIConstant.editProfile}`, data);
    }

    changePassword(data: any){
        return this.baseService.post(`${APIConstant.changePassword}`, data);
    }

    getCounts(){
        return this.baseService.get(`${APIConstant.getCount}`);
    }

    swapOrder(data:any){
        return this.baseService.post(`${APIConstant.swapOrder}`, data);
    }

    swapPolishingOrder(data:any){
        return this.baseService.post(`${APIConstant.swapPolishingOrder}`, data);
    }

    completedQuantity(data:any){
        return this.baseService.post(`${APIConstant.completedQuantity}`, data);
    }
}