import { Injectable } from "@angular/core";
import { CommonConstant } from "../constant/common.constant";
import { Utility } from "../utility";

@Injectable()
export class AuthService {

    constructor() {
    }

    saveToken(token: string) {
        window.localStorage.setItem(CommonConstant.token, token);
    }

    getToken(): string {
        return window.localStorage.getItem(CommonConstant.token);
    }

    isLoggedIn(): boolean {
        return !Utility.isEmpty(this.getToken());
    }

    loggedOut(): void {
        window.localStorage.clear();
    }

}