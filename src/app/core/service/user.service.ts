import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { Observable } from "rxjs/Observable";


@Injectable()
export class UserService {

    constructor(private baseService: BaseService) {

    }

    getUser(): Observable<any> {
        return this.baseService.get(`${APIConstant.login}`);
    }

    saveUser(user: any): Observable<any> {
        return this.baseService.post(`${APIConstant.login}`, user);
    }

    updateUserAccounts(user: any): Observable<any> {
        return this.baseService.put(`${APIConstant.login}`, user);
    }

}