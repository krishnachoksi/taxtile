import { Injectable } from "@angular/core";
import { HttpRequest, HttpInterceptor, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { timer } from 'rxjs/observable/timer';
import { IonicUtilProvider } from '../../../providers/ionic-util/ionic-util';
import { LoginService } from '../../core/service/login.service';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/finally';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    timer: any;
    token:any;
    constructor(private util: IonicUtilProvider,public loginService:LoginService) {
        this.timer = timer(1000);
        this.token = JSON.parse(localStorage.getItem('.token'));
    }
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.token = JSON.parse(localStorage.getItem('.token'));
        const authReq = req.clone({
            setHeaders: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + this.token
            }
        });

        this.util.loadingShow(); 

        return next.handle(authReq).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
               if (event.body.status === 'FAILURE') {
                this.util.loadingHide();
                } else {
                    this.util.loadingHide();
                }
            }
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                if (err.status === 400 ) {
                   this.util.loadingHide();
                }
            }
        }).finally(() => {
            this.timer.subscribe(() => {
                this.util.loadingHide();
            });
        });
    }
}