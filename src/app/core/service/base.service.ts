import 'rxjs/add/operator/map';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Events } from 'ionic-angular';
@Injectable()
export class BaseService {

    userData: any;
    password: any;
    data: {email:"",password:""};
    constructor(private http: HttpClient,public events:Events) {
    }

    get<T>(url: string): Observable<T> {
        return this.http.get<T>(url);
    }

    getWithResponse<T>(url: string): Observable<any> {
        return this.http.get<T>(url, { observe: 'response' });
    }

    post<T>(url: string, data: any): Observable<T> {
        let body = "";

        for (var key in data) {
            body += key+"="+data[key]+"&";
        }
        return this.http.post<T>(url, body);
    }

    put<T>(url: string, data: any): Observable<T> {
        return this.http.put<T>(url, data);
    }

    delete<T>(url: string): Observable<T> {
        return this.http.delete<T>(url);
    }

    public getLogin()
    {
        this.userData = JSON.parse(localStorage.getItem('.user_data'));
        this.password = atob(localStorage.getItem('.password'));
        this.data = {email:this.userData['email'],password:this.password};
        console.log(this.data);
        this.events.publish('user:login_again',this.data);
    }
}

