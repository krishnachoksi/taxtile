import { Injectable } from "@angular/core";
import { BaseService } from "./base.service";
import { APIConstant } from "../constant/api.constant";
import { CommonConstant } from "../constant/common.constant";
import { Utility } from "../utility";
import { App } from 'ionic-angular';

@Injectable()
export class LoginService {

    constructor(
        public app:App,
        private baseService: BaseService) 
    {
    }

    login(data: any) {
        return this.baseService.post(`${APIConstant.login}`, data);
    }
   
    logout() {
        const obj = {
            token: Utility.getLS(CommonConstant.token)
        }
        return this.baseService.post(`${APIConstant.logout}`, obj);
    }
}