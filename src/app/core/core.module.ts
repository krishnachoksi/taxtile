import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { LoginService } from './service/login.service';
import { BaseService } from './service/base.service';
import { AuthService } from './service/auth.service';
import { HttpModule } from '@angular/http';
import { UserService } from './service/user.service';
import { CommonService } from './service/common.service';
import { HomeService } from './service/home.service';

@NgModule({
    imports: [
        HttpModule,
        HttpClientModule
    ],
    providers: [
        CommonService,
        LoginService,
        BaseService,
        AuthService,
        UserService,
        HomeService
    ]
})
export class CoreModule { }
