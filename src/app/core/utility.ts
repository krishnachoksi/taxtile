import { URLSearchParams } from '@angular/http';
import { CommonConstant } from './constant/common.constant';

export class Utility {
    
    static getUserType(_type){
        let type = {admin:"Admin",marketing:"Marketing",digitalmachine:"Digital Machine",polishingmachine:"Polishing Machine"};
        return type[_type];
    }

    static isNull(item): boolean {
        return item === undefined || item === null;
    }

    static isEmpty(item): boolean {
        return item === undefined || item === null || item.length === 0 || item === 0 || item === '' || item === 'null';
    }

    static convertObjectToParams(paramObj: object) {
        let params = new URLSearchParams();
        for (let key in paramObj) {
            if (paramObj.hasOwnProperty(key)) {
                params.set(key, paramObj[key])
            }
        }
        return params;
    }

    static setLS(key,value)
    {
        localStorage.setItem(CommonConstant.appConst+"."+key,value);
    }

    static getLS(key):string
    {
        if(localStorage.getItem(CommonConstant.appConst+"."+key)==undefined)
        {
            return null;
        }
        else
        {
            return localStorage.getItem(CommonConstant.appConst+"."+key);
        }
    }

    static removeLS(key)
    {
        localStorage.removeItem("."+key);
    }
}