// let basePath = "http://local.taxtile.com/api/";
let basePath = "http://139.59.8.73/taxtile-backend/public/api/";

export const APIConstant = {
    basePath: basePath,
    login: `${basePath}auth/token`,
    logout: `${basePath}logout`,
    //add
    addMember: `${basePath}saveMember`,
    addOrder: `${basePath}saveOrder`,
    addProduct: `${basePath}saveProduct`,
    sendMail: `${basePath}forgot-password`,
    //get
    getAllMembers: `${basePath}getMembers`,
    getAllOrders: `${basePath}getOrders`,
    getAllProducts: `${basePath}getProducts`,
    getCount: `${basePath}getCount`,
    getOrderByUserId: `${basePath}getOrderByUserId?id=`,
    changeOrderStatus: `${basePath}changeOrderStatusById`,

    deleteMember: `${basePath}deleteMember`,
    deleteProduct: `${basePath}deleteProduct`,
    deleteOrder: `${basePath}deleteOrder`,
    editProfile: `${basePath}updateProfile`,
    changePassword: `${basePath}changePassword`,

    swapOrder: `${basePath}swapOrder`,
    swapPolishingOrder: `${basePath}swapPolishingOrder`,
    completedQuantity:`${basePath}completedQuantity`,
}