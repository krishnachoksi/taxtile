import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { NotificationPage } from '../pages/notification/notification';

import { MembersPage } from '../pages/members/members';
import { AddMemberPage } from '../pages/add-member/add-member';

import { OrdersPage } from '../pages/orders/orders';
import { AddOrderPage } from '../pages/add-order/add-order';

import { ProductsPage } from '../pages/products/products';
import { AddProductPage } from '../pages/add-product/add-product';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

//import core module
import { CoreModule } from './core/core.module';

//import service
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { IonicUtilProvider } from '../providers/ionic-util/ionic-util';

//import interpretor
import { AuthInterceptor } from './core/service/auth-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { ModalPage } from '../pages/modal/modal';

//import { CommonProvider } from '../providers/common/common';

import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
	declarations: [
		MyApp,
		HomePage,
		LoginPage,
		SignupPage,
		NotificationPage,
		MembersPage,
		AddMemberPage,
		OrdersPage,
		AddOrderPage,
		ProductsPage,
		AddProductPage,
		EditProfilePage,
		ChangePasswordPage,
		ForgotPasswordPage,
		ModalPage
	],
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp),
		HttpModule,
		IonicStorageModule.forRoot({
			name: '__ionic3_start_theme',
			driverOrder: ['indexeddb', 'sqlite', 'websql']
		}),
		CoreModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		SelectSearchableModule
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		HomePage,
		LoginPage,
		SignupPage,
		NotificationPage,
		MembersPage,
		AddMemberPage,
		OrdersPage,
		AddOrderPage,
		ProductsPage,
		AddProductPage,
		EditProfilePage,
		ChangePasswordPage,
		ForgotPasswordPage,
		ModalPage
	],
	providers: [
		StatusBar,
		SplashScreen,
		AuthServiceProvider,
		IonicUtilProvider,
		{ provide: ErrorHandler, useClass: IonicErrorHandler },
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true
		}
	]
})
export class AppModule { }
