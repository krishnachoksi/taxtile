import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class AuthServiceProvider 
{
    api_url: string;
    loading : any;
    toast : any;
    token: any;
    constructor(
        public http: Http)
    {
        // this.api_url = "http://local.taxtile.com/api/";
        this.api_url = "http://139.59.8.73/taxtile-backend/public/api/";
        this.token = localStorage.getItem('.token');
    }

    /**
     * Call post method or API from server for get data or post dara
     * @param methodName 
     * @param data 
     */
    requestPost(methodName,data) {
        return new Promise(resolve =>  {
            let headers = new Headers({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            if( this.token != ''){
                headers.append('Authorization', 'Bearer ' + this.token);
            }
        
            let options = new RequestOptions({
                headers: headers
            });

            let body = "";

            for (var key in data) {
                body += key+"="+data[key]+"&";
            }
            this.http.post(this.api_url+methodName, body, options)
            .subscribe(data => {
                console.log(data['_body']);
                resolve(JSON.parse(data['_body']));
            },error =>  {
                resolve({"error":"404"});
            });
        });
    } 
    
    /**
     * Call get method or API from server for get data or post dara
     * @param method_name 
     */
    requestGet(method_name) {
        return new Promise(resolve => {
            this.http.get(this.api_url+method_name)
            .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },error =>  {
                resolve({"error":"404"});
            });
        });
    }

    requestGetFullUrl(url) {
        return new Promise(resolve => {
            this.http.get(url)
            .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            });
        });
    }
}