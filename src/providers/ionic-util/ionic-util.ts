import { Injectable } from '@angular/core';
import { LoadingController,ToastController } from 'ionic-angular';

@Injectable()
export class IonicUtilProvider 
{
    loading : any;
    toast : any;
    showLoading = true;
    loginFail = false;
    
	constructor(
        public loadingCtrl:LoadingController,
        private toastCtrl: ToastController)
	{
    	console.log('Hello IonicUtilProvider Provider');
  	}

    /**
     * Show loading process when start any activity
     * @param spinner 
     * @param content 
     * @param showBackdrop 
     */
  	loadingShow(spinner="crescent",content="Please wait...",showBackdrop=true)
    {
        //Possible value of spinner argument => ios,ios-small,bubbles,circles,crescent,dots
        if (this.showLoading) {
            this.showLoading = false;
            this.loading = this.loadingCtrl.create({
                content: 'Please wait...',
                dismissOnPageChange: true
            });
    
            this.loading.present();
        }
    }
    
    /**
     * Hide loading process when end any activity
     */
    loadingHide() {
        this.loading.dismiss();
        this.showLoading = true;
    }
    
    showToast()
    {
        this.toast = this.toastCtrl.create({
            message: 'Retrieving...',
            position: 'middle'
        });

        this.toast.present();
    }

    /**
     * Show toast message box when inform to user for action
     */
    showToastMsg(msg)
    {
        this.toast = this.toastCtrl.create({
            message: msg,
            position: 'top',
            duration: 3000
        });

        this.toast.present();    
    }

    showToastAlert(message)
    {
        this.toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });

        this.toast.present();
    }

    /**
     * hide toast message 
     */
    hideToast()
    {
        this.toast.dismiss();
    }
}